# Cloudera Open edX Brand Package

This project contains the branding assets and style used for MFEs deployed to Cloudera.


## Installation

Install this in the MFE node environment under the `@edx/brand` alias.
Eg. `npm install --no-save '@edx/brand@git+https://gitlab.com/opencraft/client/cloudera/cloudera-mfe-brand.git#master'`


## Files this package must make available

- `/logo.svg`
- `/logo-trademark.svg` A variant of the logo with a trademark ® or ™. Note: This file must be present. If you don't have a trademark variant of your logo, copy your regular logo and use that.
- `/logo-white.svg` A variant of the logo for use on dark backgrounds
- `/favicon.ico` A site favicon
- `/paragon/fonts.scss`, `/paragon/_variables.scss`, `/paragon/_overrides.scss`  A SASS theme for `@edx/paragon <https://github.com/edx/paragon>`_. Theming documentation in Paragon is coming soon. In the meantime, you can start a theme by the contents of `_variables.scss (after line 7) <https://github.com/edx/paragon/blob/master/scss/core/_variables.scss#L7-L1046>`_ file from the Paragon repository into this file.

## License

The code files in this repository are licensed under the AGPL3 (see the LICENSE file).
The Cloudera logo images and Neue Plak fonts are proprietary.
